
#include <stdint.h>

#include "stm32f10x_rcc.h"
#include "stm32f10x_gpio.h"

#include "gendef.h"
#include "gpio.h"
#include "usb_lib.h"

GPIO_InitTypeDef GPIO_InitStructure;

void but_Init(void)
{
    RCC_APB2PeriphClockCmd(but_port_rcc , ENABLE);

    GPIO_WriteBit(but_port,but_pin,Bit_SET);
    GPIO_InitStructure.GPIO_Pin =  but_pin;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOC, &GPIO_InitStructure);
}

void led_Init(void)
{
    RCC_APB2PeriphClockCmd(led_port_rcc , ENABLE);

    GPIO_WriteBit(led_port,led_pin,Bit_SET);
    GPIO_InitStructure.GPIO_Pin =  led_pin;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOC, &GPIO_InitStructure);
}

void led_On(void)
{
	GPIO_WriteBit(led_port,led_pin,Bit_SET);
}

void led_Off(void)
{
	GPIO_WriteBit(led_port,led_pin,Bit_RESET);
}

int but_Read(void)
{
	return GPIO_ReadInputDataBit(but_port, but_pin);
}

void led_Toggle(void)
{
	static int led_status = 0;

	if ( led_status == 0 ) {
		led_status = 1;
		led_On();
	}
	else {
		led_status = 0;
		led_Off();
	}
}
