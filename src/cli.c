
#define DEBUG 1

#include <stdint.h>

#include  "cli.h"
#include  "cdc.h"
#include  "gpio.h"
#include  "util.h"

// Some GNU preprocessor tricks to convert tokens to strings
#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)

enum eEscapeModes	{ ESCNONE, ESC1, ESC2, ESC3, ESC4, ESC5 };

char				cli_aCmdBuf[cli_CMDBUFSIZE];	// buffer to hold raw input
uint8_t				cli_CmdBufIndex = 0;			// current location of cursor in command buffer
uint8_t				cli_CmdBufLen = 0;				// current length of the item in the command buffer
uint8_t				cli_CmdBufOldIndex = 0;			// old location of cursor in command buffer (used for efficiently overwriting text on the screen)
uint8_t				cli_CmdBufOldLen = 0;			// old length of the item in the command buffer
enum eEscapeModes	cli_CmdBufEscMode = ESCNONE;	// keeps track of the escape-code handling state we're in

uint8_t				cli_CursorX = 0;				// current X coord of the screen cursor
uint8_t				cli_CursorY = 0;				// current y coord of the screen cursor

uint8_t		cli_ArgCount = 0;						// amount of arguments in current commandline
char		*cli_apArgs[ cli_CMDMAXARGS ];			// pointers to arguments
uint8_t		cli_aArgOffsets[ cli_CMDMAXARGS ];

char		cli_aHistBuf[cli_HISTITEMS][cli_CMDBUFSIZE];	// history buffer
uint8_t		cli_aHistOrder[cli_HISTITEMS];					// defines order in which history items are adressed
int8_t		cli_HistNewestItem = 0;							// index to the newest history item
int8_t		cli_HistSelectedItem = 0;						// index to the currently selected history item

const char	cli_aTxtVersion[]		= cli_VERSION;
const char	cli_aTxtPrompt[]		= "monster> ";
const char	cli_aTxtError[]			= "error : ";
const char	cli_aTxtUsage[]			= "usage : ";

// error messages
const char	cli_aTxtUnknownCmd[]	= "Unknown command\n";
const char	cli_aTxtAmbigCmd[]		= "Ambigious command\n";
const char	cli_aTxtArgError[]		= "Syntax error\n";
const char	cli_aTxtNotEnoughArgs[]	= "Not enough arguments\n";
const char 	cli_aTxtTooManyArgs[]	= "Too many arguments\n";
const char	cli_aTxtArgOverflow[]	= "More than MAXARGS(" TOSTRING(cli_CMDMAXARGS) ") arguments found\n";

//=== Command array ==============================================================================
//  command     function    minargs maxargs parameterhelp   description
static const struct cli_sCmdItem cli_asCmdArray[] =
{
  { "bang",		cli_CmdBang,	1, 1,	"",				"Used to test ambiguous commands"	},
  { "bar",		cli_CmdBar,		1, 2,	"[variable]",	"Example #2"						},
  { "echo",		cli_CmdEcho,	1, 1,	"",				"Echo keys until spacebar pressed"	},
  { "help",		cli_CmdHelp,	1, 1,	"",				"Shows a list of commands"			},
  { "hist",		cli_CmdHist,	1, 1,	"",				"Display history"					},
  { "foo",		cli_CmdFoo,		1, 1,	"",				"Example #1"						},
  { "version",	cli_CmdVersion,	1, 1,	"",	 			"About monster"						},
  { NULL,		NULL,			1, 1,	NULL,	NULL										}
};

#define CURSOR 2


//================================================================================================
uint32_t cli_CmdBufGetInput( void )
{
	uint8_t  ch;

	if ( cli_RxDataIsAvail() )
	{
		ch = cli_GetChar();
#if DEBUG_TEMPLATE
		cli_PutChar( '0' );
#endif
#if CURSOR == 1
		if ( cli_CmdBufEscMode != ESCNONE )
		{
			// NOTE: This code is not used when a terminal emulator is used to connect to monster because
			// these emulators refuse to send the escape codes for arrow up/down/left/right.
			// If currently in escape-mode, then handle the current character as part of an escape code
			switch ( cli_CmdBufEscMode )				// If needed, handle the known escape codes
			{
				case ESC1:
					if ( ch == '[' )
					{
						cli_CmdBufEscMode = ESC2;
					}
					else
					{
						cli_CmdBufEscMode = ESCNONE;
					}
					break;
				case ESC2:
					if ( ch == '1' )
					{
						cli_CmdBufEscMode = ESC3;
					}
					else
					{
						cli_CmdBufEscMode = ESCNONE;
					}
					break;
				case ESC3:
					if ( ch == ';' )
					{
						cli_CmdBufEscMode = ESC4;
					}
					else
					{
						cli_CmdBufEscMode = ESCNONE;
					}
					break;
				case ESC4:
					if ( ch == '5' )
					{
						cli_CmdBufEscMode = ESC5;
					}
					else
					{
						cli_CmdBufEscMode = ESCNONE;
					}
					break;
#endif
#if CURSOR == 2
		if ( cli_CmdBufEscMode != ESCNONE )
		{
			// NOTE: This code is not used when a terminal emulator is used to connect to monster because
			// these emulators refuse to send the escape codes for arrow up/down/left/right.
			// If currently in escape-mode, then handle the current character as part of an escape code
			switch ( cli_CmdBufEscMode )				// If needed, handle the known escape codes
			{
				case ESC1:
					if ( ch == '[' )
					{
						cli_CmdBufEscMode = ESC5;
					}
					else
					{
						cli_CmdBufEscMode = ESCNONE;
					}
					break;
#endif
				case ESC5:							// must be one of the known escape codes: 'A,B,C,D' , else ignore character and reset escape mode
					cli_CmdBufEscMode = ESCNONE;	// reset ESC mode status
					switch ( ch )
					{
						case 'A':					// arrow-up
							cli_HistGetOlderItem();
							cli_PutCommand();
							break;
						case 'B':					// arrow-down
							cli_HistGetNewerItem();
							cli_PutCommand();
							break;
						case 'C':					// arrow-right
							if ( cli_CmdBufIndex < cli_CmdBufLen )		// if end of line not yet reached
							{
								cli_PutChar( cli_aCmdBuf[ cli_CmdBufIndex ] );	// reprint current character to move cursor right
								cli_CmdBufIndex++;								// move index one character towards the end
							}
							break;
						case 'D':							// arrow-left
							if ( cli_CmdBufIndex > 0 )		// if start of line not reached
							{
								cli_PutChar( ASCII_BS );
								cli_CmdBufIndex--;			// move index one character towards the start
							}
							break;
						default:
							break;
					}
					break;
				default:
					break;
			}
		}

		// if not in escape-handler mode, then just handle the character normally
		else
		{
			switch ( ch )
			{
				case ASCII_NUL:					// ignore NULLs
					break;
				case ASCII_ESC:
					cli_CmdBufEscMode = ESC1;	// activate escape mode
					break;
				case ASCII_HT:					// tab key
					cli_PutNewline();
					cli_TabHint();
					cli_PutNewPrompt();
					break;
				case ASCII_FF:					// ctrl+l means clear screen
					cli_PutChar( ASCII_FF );
					cli_PutNewPrompt();
					break;
				case ASCII_CR:					// enter
					cli_PutNewline();
					cli_HistAddItem();			// update the history buffer
					if ( cli_CmdBufParse() )	// if at least one argument was given then handle the command line
					{
						cli_CmdBufHandleCmd();
					}
//					else
//					{
//					}
					cli_CmdBufReset();
					cli_PutNewPrompt();
					break;
				case ASCII_DEL:						// delete
					cli_CmdBufDelChar();			// remove the current character from the buffer & erase it from screen
					break;
				case ASCII_BS:						// backspace
					if ( cli_CmdBufIndex > 0 )
					{
						cli_PutChar( ASCII_BS );	// move cursor one step left
						cli_CmdBufIndex--;			// move command indez one step left
						cli_CmdBufDelChar();		// now treat it exactly like a delete action
					}
					else
						cli_PutChar( ASCII_BEL );	// flash screen if buffer is full
					break;
#if TEST_CURSOR
				case ASCII_LFT:						// arrow-left
					if ( cli_CmdBufIndex > 0 )		// if start of line not reached
					{
						cli_PutChar( ASCII_BS );
						cli_CmdBufIndex--;			// move index one character towards the start
					}
					break;
				case ASCII_RT:						// arrow-right
					if ( cli_CmdBufIndex < cli_CmdBufLen )		// if end of line not yet reached
					{
						cli_PutChar( cli_aCmdBuf[ cli_CmdBufIndex ] );	// reprint current character to move cursor right
						cli_CmdBufIndex++;								// move index one character towards the end
					}
					break;
				case ASCII_UP:					// arrow-up
					cli_HistGetOlderItem();
					cli_PutCommand();
					break;
				case ASCII_DN:					// arrow-down
					cli_HistGetNewerItem();
					cli_PutCommand();
					break;
#endif
				case ASCII_ETX:					// ctrl-c ; break off and ignore the current commandline
					if ( cli_CmdBufIndex > 0 )
					{
						cli_PutNewline();
						cli_PutNewPrompt();
					}
					break;
				default:
					if ( cli_IsPrintable(ch) )	// if character is printable
					{
						cli_CmdBufInsChar(ch);	// insert character into commandline buffer
					}
				break;
			}
		}
		#if ( cli_INTERFACE == cli_USINGUSB )	// if using a USB interface, then flush the buffer now to make text appear on screen
		cdc_FlushTxBuf();						// a uart does not need this treatment, because it flushes one character at a time
		#endif
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

//================================================================================================
// Initialize the cli
void cli_Init(void)
{
#if DEBUG
	cli_PutNewline();			// just print a newline when debugging because we want to retain the previous output on screen
#else
	cli_PutChar( ASCII_FF );	// clear the screen
	cli_CursorX = 0;			// reset the cursor coords
	cli_CursorY = 0;
#endif
	cli_CmdBufReset();
	cli_HistInit();
	cli_PutNewPrompt();
}

//================================================================================================
void cli_CmdBufReset( void )
{
	cli_CmdBufLen     = 0;			// reset the commandline buffer counters to zero
	cli_CmdBufIndex   = 0;
	cli_aCmdBuf[0]    = 0;			// zero-terminate the buffer to mark it as empty
	cli_CmdBufEscMode = ESCNONE;	// set the current escape-code handler to 'no code currently being handled'
}

//================================================================================================
// Find all space-separated argument and place the address of each in the arguments array.
// Each argument is automatically zero-terminated.
bool cli_CmdBufParse( void )
{
	uint8_t index=0;

	cli_ArgCount = 0;
	while (  index < cli_CmdBufLen  )				// walk through the command buffer
	{
		// skip spaces
		if ( cli_aCmdBuf[ index ] == ASCII_SP )
		{
			index++;
		}
		// store the adress of the current argument and then find the next argument
		else
		{
			// if maximum argumentcount has not been reached then save the address of this parameter
			if ( cli_ArgCount < cli_CMDMAXARGS )
			{
				cli_apArgs[ cli_ArgCount ] = &cli_aCmdBuf[ index ];
			}

			cli_ArgCount++;		// keep count of arguments, even if we're not saving them anymore

			index++;			// skip current character because it is certainly not a space character
			// find the end of this parameter
			while ( (index < cli_CmdBufLen) && (cli_aCmdBuf[ index ] != ASCII_SP) )
			{
				index++;
			}
			cli_aCmdBuf[ index++ ] = 0;		// zero-terminate each parameter
		}
	}

	// if no command and/or arguments found then exit false
	if ( cli_ArgCount == 0 )
	{
		return FALSE;
	}
	// check that less than MAXARGS arguments have been supplied
	if ( cli_ArgCount > cli_CMDMAXARGS )
	{
		cli_PutError( cli_aTxtArgOverflow );
		return FALSE;
	}

	return TRUE;
}

//================================================================================================
/// Execute the function belonging to the command that was given.
uint32_t cli_CmdBufHandleCmd( void )
{
	cli_Cmd_t					CmdHandler;
	bool						AmbigMatch=FALSE, GotMatch=FALSE;
	const struct cli_sCmdItem	*pCmdArray,*pBestMatch;

	// find the best matching, non-ambiguous command
	for ( pCmdArray = cli_asCmdArray ; pCmdArray->name ; pCmdArray++ )
	{
		if ( cli_StrCmp( cli_apArgs[0], pCmdArray->name ) <= 0 )
		{
			if ( GotMatch == TRUE )
			{
				AmbigMatch = TRUE;
			}
			pBestMatch = pCmdArray;
			GotMatch = TRUE;
		}
	}

	// if ambiguous commands then suggest commands
	if ( AmbigMatch == TRUE )
	{
		cli_PutError( cli_aTxtAmbigCmd );
		cli_TabHint();
		return FALSE;
	}

	// if no command found exit with error
	if ( GotMatch == FALSE )
	{
		cli_PutError( cli_aTxtUnknownCmd );
	}
	// check for minimum amount of arguments
	else if ( cli_ArgCount < pBestMatch->minargs )
	{
		cli_PutError( cli_aTxtNotEnoughArgs );
		cli_PutCommandInfo( pBestMatch );
	}
	// check for maximum amount of arguments
	else if ( cli_ArgCount > pBestMatch->maxargs )
	{
		cli_PutError( cli_aTxtTooManyArgs );
		cli_PutCommandInfo( pBestMatch );
	}
	// if nothing went wrong, then handle the command
	else
	{
		CmdHandler = pBestMatch->handler;					// get the handler
		CmdHandler( cli_ArgCount, (char **)&cli_apArgs );	// call the handler
		return TRUE;
	}

	return FALSE;
}


//================================================================================================
// Insert a character into the commandbuffer and reprint the modified part.
void cli_CmdBufInsChar( const uint8_t ch )
{
	int8_t	copycount, Index, OldLen;

	if ( cli_CmdBufLen < cli_CMDBUFSIZE-1 )				// check that buffer is not full yet
	{
		copycount =  cli_CmdBufLen - cli_CmdBufIndex;	// shift everything riqht of cursor one place right if needed
		OldLen = copycount;
		Index = cli_CmdBufLen;
		while ( copycount-- > 0 )
		{
			cli_aCmdBuf[ Index ] = cli_aCmdBuf[ Index-1 ];
			Index--;
		}
		cli_CmdBufLen++;									// increment the buffer length by one character
		cli_aCmdBuf[ cli_CmdBufLen ] = 0;					// zero-terminate the buffer so that it may be printed
		cli_aCmdBuf[ cli_CmdBufIndex ] = ch;				// insert new character at cursor position
		cli_PutString( &cli_aCmdBuf[ cli_CmdBufIndex ] );	// print the modified commandline to the screen
		while ( OldLen-- > 0)								// return cursor to current position
		{
			cli_PutChar( ASCII_BS );
		}

		cli_CmdBufIndex++;			// increment the buffer index by one character
	}
	else
	{
		cli_PutChar( ASCII_BEL );	// flash screen if buffer is full
	}
}

//================================================================================================
// Remove a character from the command buffer and reprint the modified part.
void cli_CmdBufDelChar( void )
{
	int8_t	Count, Index, OldLen;

	if ( cli_CmdBufIndex != cli_CmdBufLen )		// don't do anything if cursor is at end of line
	{
		if ( cli_CmdBufLen > 0)					// check if there is something to delete
		{
			Count = (cli_CmdBufLen - cli_CmdBufIndex) - 1;	// copy everything riqht of cursor one place left
			OldLen = Count;
			Index = cli_CmdBufIndex;
			while ( Count-- > 0 )
			{
				cli_aCmdBuf[ Index ] = cli_aCmdBuf[ Index+1 ];
				Index++;
			}
			cli_aCmdBuf[ Index ] = ASCII_SP;					// add a space, which will printed to overwrite the deleted character
			cli_aCmdBuf[ Index+1 ] = 0;							// zero terminate string so that it may be printed
			cli_PutString( &cli_aCmdBuf[ cli_CmdBufIndex ] );	// print the modified commandline
			cli_aCmdBuf[ Index ] = 0;							// remove the space character which was only added for erasing a character
			while ( OldLen-- >= 0)								// return cursor to current position
			{
				cli_PutChar( ASCII_BS );
			}

			cli_CmdBufLen--;
		}
	}
}

//=== History handling ===========================================================================

//================================================================================================
// Initialize the history buffer to mark it as completely empty
void cli_HistInit( void )
{
	uint8_t Index;

	for ( Index = 0 ; Index < cli_HISTITEMS ; Index++ )
	{
		cli_aHistBuf[Index][0] = 0;		// zero-out every first byte of each history item, to mark it as emtpy
		cli_aHistOrder[Index] = Index;	// initially populate the the buffer index incrementally
	}
}

//================================================================================================
// Copy a line from the command buffer to the newest location in the history buffer
void cli_HistCopyCmdBufToNew( void )
{
	int8_t	BufNumber;

	BufNumber = cli_aHistOrder[ cli_HistNewestItem ];
	cli_StrnCopy(cli_aCmdBuf , &cli_aHistBuf[ BufNumber ][ 0 ], cli_CMDBUFSIZE);
}

//================================================================================================
// Copy a line from the command buffer to the newest location in the history buffer
void cli_HistCopyCmdBufToSelected( void )
{
	int8_t	BufNumber;

	BufNumber = cli_aHistOrder[ cli_HistSelectedItem ];
	cli_StrnCopy(cli_aCmdBuf , &cli_aHistBuf[ BufNumber ][ 0 ], cli_CMDBUFSIZE);
}

//================================================================================================
// Copy the selected line from the history buffer to the command buffer
void cli_HistCopySelectedToCmdBuf( void )
{
	int8_t	BufNumber;

	BufNumber = cli_aHistOrder[ cli_HistSelectedItem ];
	cli_StrnCopy( &cli_aHistBuf[ BufNumber ][ 0 ], cli_aCmdBuf, cli_CMDBUFSIZE);
}

//================================================================================================
///Name:
// Registers the current command buffer in the history buffer.
//
// If the current command buffer was retreived from history and is still unmodified,
// then it is not copied to the history buffer again, as that would create a duplicate
// item in the index.  Instead the history index buffer is just reordered so that.
// This is 'nice to have' feature limits the entry of duplicate entries in the history buffer.
void cli_HistAddItem( void )
{
	uint8_t	SelBufNumber=0;
	uint8_t	PrevBufItem=0;
	uint8_t	Index=0;
	uint8_t	OldIndex=0;
	uint8_t	NewIndex=0;

	// get the index of the youngest history item
	PrevBufItem = cli_HistNewestItem-1;
	if ( PrevBufItem < 0 )
	{
		cli_HistSelectedItem = cli_HISTITEMS-1;
	}

	// get the real offset of the selected history item
	SelBufNumber  = cli_aHistOrder[ cli_HistSelectedItem ];

	// if the current commandbuffer was retreived from history and is still unmodified
	// then just re-arrange the indexes of the history-buffer. This reduces the occurance
	// of duplicate items in the history. Indexes are used to speed up the process of
	// rearranging the order of history items because it's lots faster than copyin around
	// all the actual strings in the history buffer.
	if ( (cli_HistSelectedItem != cli_HistNewestItem ) &&
		 ( cli_StrCmp( cli_aCmdBuf , &cli_aHistBuf[ SelBufNumber ][ 0 ] ) == 0))
	{
		NewIndex = cli_HistSelectedItem;
		while ( NewIndex != cli_HistNewestItem )
		{
			OldIndex = NewIndex++;
			if ( NewIndex == cli_HISTITEMS )
			{
				NewIndex = 0;
			}
			cli_aHistOrder[ OldIndex ] = cli_aHistOrder[ NewIndex ];
		}
		// and finally place the selected history item in the most recent entry of the history queue
		cli_aHistOrder[ OldIndex ] = SelBufNumber;
	}

	// check if the current command is not equal to the previous history item
	else if ( cli_StrCmp( cli_aCmdBuf , &cli_aHistBuf[ PrevBufItem ][ 0 ] ) != 0 )
	{
		// skip all leading spaces
		for ( Index = 0; (Index < cli_CmdBufLen) && (cli_aCmdBuf[Index] == ASCII_SP); Index++ )
		{
			// NOP loop
		}
		// if the command length is non-zero, then add it to the history
		if ( Index < cli_CmdBufLen )
		{
			cli_HistCopyCmdBufToNew();
			// now increment and clip the index to the newest item
			cli_HistNewestItem++;
			if ( cli_HistNewestItem == cli_HISTITEMS )
			{
				cli_HistNewestItem = 0;
			}
		}
	}
	// make current selected item equal to the most recent added item
	cli_HistSelectedItem = cli_HistNewestItem;
}

//================================================================================================
void cli_HistGetOlderItem( void )
{
	int8_t BufNumber, OldSelected;

	cli_HistCopyCmdBufToSelected();			// store the current line to the currently selected history item
	OldSelected = cli_HistSelectedItem--;	// point to an older command
	if ( cli_HistSelectedItem < 0 )
	{
		cli_HistSelectedItem = cli_HISTITEMS-1;
	}
	// if oldest item reached then remain there
	if ( cli_HistSelectedItem == cli_HistNewestItem )
	{
		cli_HistSelectedItem = OldSelected;
	}
	// else recover the history item
	else
	{
		BufNumber = cli_aHistOrder[ cli_HistSelectedItem ];		// translate index to a real buffer number
		if ( cli_aHistBuf[BufNumber][0] != 0 )					// if history item if it is not empty
		{
			cli_HistCopySelectedToCmdBuf();			// copy selected history item to the commandline
		}
		else
		{
			cli_HistSelectedItem = OldSelected;		// else remain at the previous history item
		}
	}
}

//================================================================================================
void cli_HistGetNewerItem( void )
{
	cli_HistCopyCmdBufToSelected();		// store the current line to the currently selected history item

	if ( cli_HistSelectedItem != cli_HistNewestItem )	// if we're not at the youngest history item
	{
		cli_HistSelectedItem++;							// point to a younger history item
		if ( cli_HistSelectedItem == cli_HISTITEMS )
		{
			cli_HistSelectedItem = 0;
		}
		cli_HistCopySelectedToCmdBuf();		// and copy it to the commandline
	}
}

//================================================================================================
// Compare all avalable commands with the buffer and display the name, usage and help for all matches

//XXX: make it ignore leading spaces: this should be done by cmdbuf parse
void cli_TabHint( void )
{
	const struct cli_sCmdItem	*pCmdArray, *pBestMatch;
	bool						GotMatch=FALSE, AmbigMatch=FALSE;
	char						*pCmdString;

	// skip initial spaces
	pCmdString = cli_aCmdBuf;
	while ( (*pCmdString == ASCII_SP) && (*pCmdString != 0 ) )
	{
		pCmdString++;
	}

	// walk through the command array
	for ( pCmdArray = cli_asCmdArray ; pCmdArray->name ; pCmdArray++ )
	{
		// print help text for all commands in command-length is zero or for all commands that matches the command buffer
		if ( (cli_WordCmp( pCmdString, pCmdArray->name ) <= 0) || (*pCmdString == 0) )
		{
			if ( GotMatch == TRUE )
			{
				AmbigMatch = TRUE;
			}
			pBestMatch = pCmdArray;					// store the most recent match
			cli_PutCommandInfo( pBestMatch );
			GotMatch = TRUE;
		}
	}

	if ( GotMatch == FALSE )
	{
		cli_PutString("No matching command exists.\n");
	}
}

//================================================================================================
// Prints out the name, usage and help information for a given pointer to a cmdarray
void cli_PutCommandInfo( const struct cli_sCmdItem *pCmdArray )
{
	cli_PutString( pCmdArray->name );
	cli_PutChar( ' ' );
	cli_PutString( pCmdArray->usage );
	cli_PutTab( 20 );
	cli_PutString( " : " );
	cli_PutString( pCmdArray->help );
	cli_PutNewline();
}

void cli_PutNewPrompt( void )
{
	cli_PutString( cli_aTxtPrompt );
	cli_PutString( cli_aCmdBuf );
}

//================================================================================================
// Prints the current string in the command buffer while clearing out the old text
// It expects the command buffer to contain the new text and the BufLen and BufIndex to
// still belong to the previous contents of the buffer. Using this info, the line
// can be redrawn while minimizing the amount of output spaces.
void cli_PutCommand( void )
{
	int8_t	Count, OldLen;

	OldLen = cli_CmdBufLen;			// store this to use it later for erasing remaining characters
	Count = cli_CmdBufIndex;		// move cursor to beginning of commandline on screen
	while ( Count-- > 0 )
	{
		cli_PutChar( ASCII_BS );
	}

	cli_CmdBufLen = cli_PutString( cli_aCmdBuf );	// print it to the screen

	Count = OldLen - cli_CmdBufLen;		// erase the remaining characters on screen, if any
	while ( Count-- > 0 )
	{
		cli_PutChar( ASCII_SP );
	}
	Count = OldLen - cli_CmdBufLen;		// place cursor back at the end of the commandline
	while ( Count-- > 0 )
	{
		cli_PutChar( ASCII_BS );
	}
	cli_CmdBufIndex = cli_CmdBufLen;
}

//================================================================================================
// Prints a string prefixed by a fixed error string
void cli_PutError( const char *pErrorString )
{
	cli_PutString( cli_aTxtError );
	cli_PutString( pErrorString );
}

//================================================================================================
void cli_ClearLine ( void )
{
	uint8_t	count;

	cli_PutChar( ASCII_CR );
	for ( count = 0 ; count < cli_CMDBUFSIZE-1 ; count++)
	{
		cli_PutChar( ASCII_SP );
	}
	cli_PutChar( ASCII_CR );
}

//=== String & single-character printing and handling stuff here =================================

//================================================================================================
// Name: cli_PutChar
// Puts a character in the output interface's buffer. Also keeps track of the
// horizontal coordinate cursor. This can be used to print characters at fixed
// places on the screen.
//================================================================================================
void cli_PutChar( const char ch )
{
#if cli_INTERFACE == cli_USINGUART
	uart_PutChar( ch );
#else
	cdc_PutChar( ch );
#endif
	switch ( ch )
	{
	case ASCII_CR:			// if carriage return, zero-out cursor x coord
		cli_CursorX = 0;
		break;
	case ASCII_FF:			// if formfeed, zero-out cursor x & y coord
		cli_CursorX = 0;
		cli_CursorY = 0;
		break;
	case ASCII_LF:			// if line feed, increment cursor y coord
		cli_CursorY++;
		break;
	case ASCII_BS:			// if backspace, decrement cursor x coord
		cli_CursorX--;
		break;
	default:
		if ( cli_IsPrintable(ch) )
		{
			cli_CursorX++;	// if printable character, increment cursor x coord
		}
		break;
	}
}

//================================================================================================
void cli_PutNewline( void )
{
	cli_PutChar( ASCII_LF );
	cli_PutChar( ASCII_CR );
}

//================================================================================================
void cli_PutTab( const uint8_t xcoord )
{
	while ( cli_CursorX < xcoord )
	{
		cli_PutChar( ASCII_SP );
	}
}

//================================================================================================
uint8_t cli_PutString( const char *pString )
{
	char ch;
	uint8_t len=0;

	ch = *pString;
	while ( ch != 0 )
	{
		if ( ch == ASCII_LF )	// translate line feeds to LF + CR
		{
			cli_PutNewline();
		}
		else
		{
			cli_PutChar( ch );
		}
		len++;
		pString++;
		ch = *pString;
	}
	return len;
}

//================================================================================================
void  cli_PutHexDigit( uint8_t Nibble )
{
	Nibble &= 0x0f;
	if ( Nibble < 10 )
	{
		cli_PutChar ( '0' + Nibble );
	}
	else
	{
		cli_PutChar ( 'A' + Nibble - 10 );
	}
}

//================================================================================================
void  cli_PutHexByte( const uint8_t Byte )
{
	cli_PutHexDigit( Byte >> 4 );
	cli_PutHexDigit( Byte );
}

//================================================================================================
// Description: Copies at most maxlen byte from zero-terminated string from src to dst.
// Returns the length of the copied string.
uint8_t cli_StrnCopy( const char *pSrc, char *pDst, uint32_t maxlen )
{
	uint8_t len=0;

	while ( (maxlen-- > 0) && (*pSrc != 0) )
	{
		*pDst = *pSrc;
		pSrc++;
		pDst++;
		len++;
	}
	*pDst = 0;			// zero-terminate the destination
	return len;			// return the length of the copied string (minus termintaion zero)
}

//================================================================================================
// Returns TRUE if the zero-terminated string *src completely matches the first part of dst.
//XXX: This routine does not act like strcmp: Fix it to act even more different:
// 0 different -1 str1 complete 1 str2 complete
int8_t cli_StrCmp( const char *pStr1, const char *pStr2 )
{
	while ( (*pStr1 == *pStr2) && (*pStr1 != 0) && (*pStr2 != 0) )
	{
		pStr1++;
		pStr2++;
	}

	// if both strings match
	if ( (*pStr1 - *pStr2) == 0 )
	{
		return 0;
	}
	// if str1 was completely matched
	else if ( *pStr1 == 0 )
	{
		return -1;
	}
	// else if str2 was completely matched
	else
	{
		return 1;
	}
}


//================================================================================================
// Returns a value indicating the relationship between the strings:
// A zero value indicates that the characters compared in both strings are all equal.
// A value greater than zero indicates that the first character that does not match has a greater
// value in str1 than in str2; And a value less than zero indicates the opposite.
// NOTE: NULL and space are both considered word terminators.
int8_t cli_WordCmp( const char *pStr1, const char *pStr2 )
{
	while ( (*pStr1 == *pStr2) && (*pStr1 != 0) && (*pStr2 != 0) && (*pStr1 != ASCII_SP) && (*pStr2 != ASCII_SP) )
	{
		pStr1++;
		pStr2++;
	}

	// if both strings match
	if ( (*pStr1 - *pStr2) == 0 )
	{
		return 0;
	}
	// if str1 was completely matched
	else if (( *pStr1 == 0 ) || ( *pStr1 == ASCII_SP ))
	{
		return -1;
	}
	// else if str2 was completely matched
	else
	{
		return 1;
	}
}


//================================================================================================
// Returns a value indicating the relationship between the strings:
// A zero value indicates that the characters compared in both strings are all equal.
// A value greater than zero indicates that the first character that does not match has a greater
// value in str1 than in str2; And a value less than zero indicates the opposite.
// NOTE: NULL is considered to be the string terminator.
int8_t cli_StrnCmp( const char *pStr1, const char *pStr2, uint8_t maxlen )
{
	while ( (*pStr1 == *pStr2) && (*pStr1 != 0) && (*pStr2 != 0) && (maxlen > 0) )
	{
		pStr1++;
		pStr2++;
		maxlen--;
	}

	// if both strings match or max length was reached, then signal a match
	if ( (maxlen == 0) || ( (*pStr1 - *pStr2) == 0) )
	{
		return 0;
	}
	// if str1 was completely matched
	else if ( *pStr1 == 0 )
	{
		return -1;
	}
	// else if str2 was completely matched
	else
	{
		return 1;
	}
}

//================================================================================================
bool cli_IsDecimal( const char ch )
{
	return ( (ch >= '0') && (ch <= '9') );
}

//================================================================================================
bool cli_IsPrintable( const char ch )
{
	return ( (ch >= ASCII_SP ) && (ch <= 126) );
}

//================================================================================================
uint8_t cli_ToLower ( uint8_t ch )
{
	if ( ( ch >= 'A' ) && ( ch <= 'Z' ) )
	{
		ch += 32;
	}
	return ch;
}

//================================================================================================
uint8_t cli_ToUpper ( uint8_t ch )
{
	if ( ( ch >= 'a' ) && ( ch <= 'z' ) )
	{
		ch -= 32;
	}
	return ch;
}

//=== Commands go here ===========================================================================

//================================================================================================
uint32_t cli_CmdHelp( uint32_t argc, char **argv )
{
	const struct cli_sCmdItem	*pCmdArray;

	cli_PutString( "\nAvailable commands:\n");
	for ( pCmdArray = cli_asCmdArray ; pCmdArray->name ; pCmdArray++ )
	{
		cli_PutCommandInfo( pCmdArray );
	}
	return 0;
}

//================================================================================================
uint32_t cli_CmdVersion( uint32_t argc, char **argv )
{
	cli_PutString( cli_aTxtVersion );
	return 0;
}

//================================================================================================
// An example routine
uint32_t cli_CmdFoo( uint32_t argc, char **argv )
{
	cli_PutString( "This is foo.\n" );
	return 0;
}

//================================================================================================
// An example routine
uint32_t cli_CmdBar( uint32_t argc, char **argv )
{
	if ( argc < 2 )
	{
		cli_PutString( "Bar has nothing to fix." );
	}
	else
	{
		cli_PutString( "Bar fixed the following item : " );
		cli_PutString( argv[1] );
	}
	cli_PutNewline();
	return 0;
}

//================================================================================================
// An example routine
uint32_t cli_CmdBang( uint32_t argc, char **argv )
{
	cli_PutString( "Boo!\n" );
	return TRUE;
}

//================================================================================================
// NOTE: This is an example routine that displays the current history buffer.
uint32_t cli_CmdHist( uint32_t argc, char **argv )
{
	uint8_t	BufNumber;
	int8_t	Selected, Count;

	Selected = cli_HistNewestItem;
	Count = cli_HISTITEMS;

	while ( Count > 0 )
	{
		Count--;
		Selected--;
		if ( Selected < 0 )
		{
			Selected = cli_HISTITEMS-1;
		}
		BufNumber = cli_aHistOrder[ Selected ];			// translate index to a real buffer number

		if ( cli_aHistBuf[BufNumber][0] == 0 )			// when encountering an empty history string
		{
			Count = 0;									// then stop waling thwough the history buffer
		}
		else
		{
			cli_PutString( (const char*)&cli_aHistBuf[BufNumber] );	// else print the history line
			cli_PutNewline();
		}
	}
	return 0;
}

uint32_t cli_CmdEcho( uint32_t argc, char **argv )
{
	uint8_t ch = 0;

	cdc_FlushTxBuf();						// a uart does not need this treatment, because it flushes one character at a time
	while ( ch != ASCII_CR )
	{
		if ( cli_RxDataIsAvail() )
		{
			ch = cli_GetChar();
			if ( ch != ASCII_CR )
			{
				cli_PutHexByte( ch );
				if ( cli_IsPrintable(ch) )
				{
					cli_PutChar('-');
					cli_PutChar(ch);
				}
				cli_PutNewline();
				cdc_FlushTxBuf();						// a uart does not need this treatment, because it flushes one character at a time
			}
		}
	}
	return 0;
}


//=== EoF ===

