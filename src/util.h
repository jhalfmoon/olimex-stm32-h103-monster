
#ifndef  _UTIL_H_
#define  _UTIL_H_

#define	BIT_0	0x01
#define	BIT_1	0x02
#define	BIT_2	0x04
#define	BIT_3	0x08
#define	BIT_4	0x10
#define	BIT_5	0x20
#define	BIT_6	0x40
#define	BIT_7	0x80
#define	BIT_8	0x0100
#define	BIT_9	0x0200
#define	BIT_10	0x0400
#define	BIT_11	0x0800
#define	BIT_12	0x1000
#define	BIT_13	0x2000
#define	BIT_14	0x4000
#define	BIT_15	0x8000


#define	ASCII_NUL	0
#define	ASCII_ETX	3	// ctrl-c, Break
#define	ASCII_ACK	6
#define	ASCII_BEL	7	// beep / flash
#define	ASCII_BS	8	// ctrl+h, Backspace
#define	ASCII_HT	9	// tab
#define	ASCII_LF	10
#define	ASCII_FF	12	// ctrl+l, formfeed
#define	ASCII_CR	13
#define	ASCII_DC2	18	// ctrl+r, device control 2
#define	ASCII_NAK	21
#define	ASCII_CAN	24	// ctrl+x, cancel line
#define	ASCII_ESC	27
#define	ASCII_SP	32

// NOTE: These are defined for debugging purposes only. The characters ASCII_* are temporary replacements until the up/down/left/right escape code
// handling is finally working.
#define	ASCII_LFT	'0'+4
#define	ASCII_RT	'0'+6
#define	ASCII_UP	'0'+8
#define	ASCII_DN	'0'+2
#define	ASCII_DEL	'0'+5

#endif	/* _UTIL_H_ */

