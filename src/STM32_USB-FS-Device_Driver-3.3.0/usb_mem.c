/******************** (C) COPYRIGHT 2011 STMicroelectronics ********************
* File Name          : usb_mem.c
* Author             : MCD Application Team
* Version            : V3.3.0
* Date               : 21-March-2011
* Description        : Utility functions for memory transfers to/from PMA
********************************************************************************
* THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
* WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE TIME.
* AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY DIRECT,
* INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE
* CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING
* INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
*******************************************************************************/
#ifndef STM32F10X_CL

/* Includes ------------------------------------------------------------------*/
#include "usb_lib.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Extern variables ----------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
/*******************************************************************************
* Function Name  : UserToPMABufferCopy
* Description    : Copy a buffer from user memory area to packet memory area (PMA)
* Input          : - pbUsrBuf: pointer to user memory area.
*                  - wPMABufAddr: address into PMA.
*                  - wNBytes: no. of bytes to be copied.
* Output         : None.
* Return         : None	.
*******************************************************************************/
void UserToPMABufferCopy2(uint8_t *pUsrBuf, uint16_t PMABufAddr, uint16_t NBytes)
{
  uint32_t n = (NBytes + 1) >> 1;   /* n = (wNBytes + 1) / 2 */
  uint32_t i, temp1, temp2;
  uint16_t *pVal;
  pVal = (uint16_t *)(PMABufAddr * 2 + PMAAddr);
  for (i = n; i != 0; i--)
  {
    temp1 = (uint16_t) * pUsrBuf;
    pUsrBuf++;
    temp2 = temp1 | (uint16_t) * pUsrBuf << 8;
    *pVal++ = temp2;
    pVal++;
    pUsrBuf++;
  }
}

void UserToPMABufferCopy(uint8_t *pUsrBuf, uint16_t PMABufAddr, uint16_t NBytes)
{
	uint16_t	*pHwBuf;
	uint16_t	NewWord;
	uint8_t		IsOddByte;

	IsOddByte = ((uint8_t) PMABufAddr) & 1;
	pHwBuf = (uint16_t *)((PMABufAddr/2)*4 + PMAAddr);

	while ( NBytes-- != 0 )
	{
		if ( IsOddByte == 1 )
		{
			NewWord  = *pHwBuf & 0xff;
			NewWord |= ((uint16_t)*pUsrBuf << 8);
			*pHwBuf  = NewWord;
			pHwBuf  += 2;
			IsOddByte = 0;
		}
		else
		{
			NewWord   = *pHwBuf & 0xff00;
			NewWord  |= (uint16_t)*pUsrBuf;
			*pHwBuf   = NewWord;
			IsOddByte = 1;
		}
		pUsrBuf++;
	}
}

/*******************************************************************************
* Function Name  : PMAToUserBufferCopy
* Description    : Copy a buffer from user memory area to packet memory area (PMA)
* Input          : - pbUsrBuf    = pointer to user memory area.
*                  - wPMABufAddr = address into PMA.
*                  - wNBytes     = no. of bytes to be copied.
* Output         : None.
* Return         : None.
*******************************************************************************/
void PMAToUserBufferCopy(uint8_t *pUsrBuf, uint16_t PMABufAddr, uint16_t NBytes)
{
  uint32_t n = (NBytes + 1) >> 1;/* /2*/
  uint32_t i;
  uint32_t *pVal;

  pVal = (uint32_t *)(PMABufAddr * 2 + PMAAddr);
  for (i = n; i != 0; i--)
  {
    *(uint16_t*)pUsrBuf++ = *pVal++;
    pUsrBuf++;
  }
}

#endif /* STM32F10X_CL */
/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
