
#include "usb_lib.h"
#include "usb_desc.h"
#include "usb_mem.h"
#include "hw_config.h"
#include "usb_istr.h"

//extern __IO uint32_t	cdc_bTxDone;
extern __IO uint32_t	cdc_bRxGotData;
extern __IO int32_t		cdc_usRxCount;

void EP3_OUT_Callback(void)
{
	cdc_usRxCount = GetEPRxCount(ENDP3);
	if ( !cdc_usRxCount )					// if empty packet received, then immediately allow next packet to be received
		SetEPRxValid(ENDP3);
//	else
//		cdc_bRxotData = 1;
}

void EP1_IN_Callback(void)
{
//	cdc_bTxDone = 1;
}
