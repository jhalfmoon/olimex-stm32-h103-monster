
#ifndef  _CDC_H_
#define  _CDC_H_

#include "usb_lib.h"

uint8_t	cdc_GetChar( void );
void	cdc_PutChar( uint8_t data );
void	cdc_FlushTxBuf( void );
bool	cdc_RxDataIsAvail( void );

// dummy routines, maybe added later
uint8_t	DBGU_Getch( void );
void	DBGU_Putch( uint8_t data);
bool	DBGU_RxDataIsAvail( void );

#endif	/* _CDC_H_ */
