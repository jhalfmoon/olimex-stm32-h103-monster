
#ifndef __MAIN_H
#define __MAIN_H

#define	MSEC_PER_TICK	1
#define	TICKS_PER_5MS	(5/MSEC_PER_TICK)
#define	TICKS_PER_10MS	(10/MSEC_PER_TICK)
#define	TICKS_PER_20MS	(20/MSEC_PER_TICK)
#define	TICKS_PER_50MS	(50/MSEC_PER_TICK)
#define	TICKS_PER_100MS	(100/MSEC_PER_TICK)
#define	TICKS_PER_200MS	(200/MSEC_PER_TICK)
#define	TICKS_PER_500MS	(500/MSEC_PER_TICK)
#define	TICKS_PER_SEC	(1000/MSEC_PER_TICK)

void Delay( __IO uint32_t nTime );
void TaskDispatcher( void );
void PutRaw( void );

#endif /* __MAIN_H */
