
#include "stm32f10x.h"
#include "hw_config.h"
#include "usb_lib.h"
#include "usb_desc.h"
#include "usb_pwr.h"

#include "cdc.h"
#include "gpio.h"
#include "main.h"
#include "cli.h"
#include "util.h"
// system_stm32f10x.c

__IO uint32_t	ulSystick_DownCount = 0;
__IO uint32_t	ulSystick_UpCount = 0;
__IO uint32_t	ulSystick_MillisecCount = 0;

__IO bool		bSystick_5msFlag;
__IO bool		bSystick_50msFlag;
__IO bool		bSystick_500msFlag;

uint32_t testy ( uint32_t );

void main( void )
{
	Set_System();
	SysTick_Config(SystemCoreClock / 1000);
	Set_USBClock();
	USB_Interrupts_Config();
	USB_Init();
	led_Init();
	but_Init();
//	USB_Cable_Config( DISABLE ) ;
//	Delay ( 1000 );
//	USB_Cable_Config( ENABLE ) ;
	while ( ! bDeviceState == CONFIGURED ) {};

	cli_Init();
	while (1)
	{
		TaskDispatcher();
		cli_CmdBufGetInput();
//		PutRaw();
	}
}

// Routines that will be loaded to RAM instead of FLASH from here on.
// It is used to test debugging in RAM, which is done to prevent
// quick wear of the FLASH memory by constant re-flashing.

//__attribute__ ((section(".ramtext")))
void PutRaw ( void )
{
	uint8_t c = 0;

	if ( cdc_RxDataIsAvail )
	{
		c = cdc_GetChar();
		if ( c )
		{
			led_Toggle();
			cli_PutHexByte(c);
			cdc_FlushTxBuf();
		}
	}
}

//__attribute__ ((section(".ramtext")))
void TaskDispatcher( void )
{
	static uint8_t c = 0;

	if ( bSystick_5msFlag )        // Do 5mSec periodic task(s)...
	{
		bSystick_5msFlag = 0;
	}
	if ( bSystick_50msFlag )       // Do 50mSec periodic task(s)...
	{
		led_Toggle();
		bSystick_50msFlag = 0;
	}
	if ( bSystick_500msFlag )      // Do 500mSec periodic task(s)...
	{
		led_Toggle();
		led_Toggle();
		led_Toggle();
		bSystick_500msFlag = 0;
	}
}

//__attribute__ ((section(".ramtext")))
void Delay(__IO uint32_t nTime)
{
	ulSystick_DownCount = nTime;

	while(ulSystick_DownCount != 0)  {} ;
}

__attribute__ ((section(".ramtext")))
uint32_t  millisecTimer( void )
{
	uint32_t count = 32;
	while ( count-- ) ;
	while ( count-- ) ;
	return  ulSystick_MillisecCount;
}


/*** EOF ***/

