
#include <stdint.h>

#include "cdc.h"
#include "gpio.h"
#include "usb_lib.h"
#include "usb_desc.h"

volatile int8_t		cdc_usRxCount	= 0;
volatile int8_t		cdc_usTxCount	= 0;

uint8_t cdc_GetChar( void )
{
	uint8_t usData = 0;

	if ( cdc_usRxCount )
	{
		PMAToUserBufferCopy(&usData, ENDP3_RXADDR, 1);
		if ( --cdc_usRxCount == 0 )
		{
			SetEPRxValid(ENDP3);
		}
	}
	return usData;
}

void cdc_PutChar( uint8_t data )
{
	while ( GetEPTxStatus(ENDP1) != EP_TX_NAK ) {};			// do not mess with the buffer if a tx is still in progress
	UserToPMABufferCopy(&data, ENDP1_TXADDR + cdc_usTxCount, 1);
	if ( ++cdc_usTxCount == VIRTUAL_COM_PORT_DATA_SIZE )
	{
		cdc_FlushTxBuf();
	}
}

void cdc_FlushTxBuf( void )
{
	while ( GetEPTxStatus(ENDP1) != EP_TX_NAK ) {};			// do not mess with the buffer if a tx is still in progress
	SetEPTxCount(ENDP1, cdc_usTxCount);
	SetEPTxValid(ENDP1);
	cdc_usTxCount = 0;
	while ( GetEPTxStatus(ENDP1) != EP_TX_NAK ) {};
	SetEPTxCount(ENDP1, 0);
	SetEPTxValid(ENDP1);
}

bool cdc_RxDataIsAvail( void )
{
	if ( cdc_usRxCount )
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

bool uart_RxDataIsAvail( void )
{
	led_Toggle();
	return 0 ;
}

uint8_t uart_GetChar( void )
{
	led_Toggle();
	return 0 ;
}

void uart_PutChar( uint8_t foobar)
{
	led_Toggle();
	while (0);
}
