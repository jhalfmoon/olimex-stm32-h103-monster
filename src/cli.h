
#ifndef  _CLI_H_
#define  _CLI_H_

// Only included for 'bool' typedef
#include "usb_lib.h"

//========================================================================

#define		cli_VERSION		"monster v0.99 - 20111114(jhalfmoon)\n";
#define		cli_USINGUSB	0
#define		cli_USINGUART	1
#define		cli_INTERFACE	USB_PORT		// define which interface is used

#if cli_INTERFACE == cli_USINGUART
#define  cli_RxDataIsAvail()	uart_RxDataIsAvail()
#define  cli_GetChar()			uart_GetChar()
#else
#define  cli_RxDataIsAvail()	cdc_RxDataIsAvail()
#define  cli_GetChar()			cdc_GetChar()
#endif

// NOTE: Keep in mind that the last byte of the command buffer is always used for zero-termination.
#define		cli_CMDBUFSIZE		41			// maximum amount of characters in the command line buffer(+zero terminator)
#define		cli_CMDMAXARGS		8			// maximum amount of allowed parameters (including the actual command)
//XXX: Maybe the comments here need to be updated / cleaned up.

// The amount of old commandlines kept in history is x-1 because the youngest history
// item is used as the buffer for the current command. In other words, the contents of the
// command buffer always gets stored in the history first before another history item is retrieved.
#define		cli_HISTITEMS		9

//========================================================================

typedef uint32_t (*cli_Cmd_t)( uint32_t argc, char **argv);

struct cli_sCmdItem
{
	const char			*name;			// name of the command
	const cli_Cmd_t		handler;		// function that handles the command
	const uint8_t		minargs;		// minimum number of arguments (including command)
	const uint8_t		maxargs;		// maximum number of arguments (including command)
	const char			*usage;			// list possible parameters
	const char			*help;			// explain what command does
};

void		cli_Init( void );
void		cli_PutNewline( void );
void		cli_PutPrompt( void );
void		cli_PutChar( const char ch );
uint8_t		cli_PutString( const char *pString );
void		cli_PutError( const char *pErrorString );
void		cli_PutNewPrompt( void );
void		cli_PutCommand( void );
void		cli_PutCommandInfo( const struct cli_sCmdItem *pCmdArray );
void		cli_PutTab( uint8_t xcoord );
void		cli_ClearLine( void );

int8_t		cli_StrCmp( const char *pSrc1, const char *pSrc2 );
int8_t		cli_WordCmp( const char *pSrc1, const char *pSrc2 );
int8_t		cli_StrnCmp( const char *pSrc1, const char *pSrc2, uint8_t maxlen );
uint8_t		cli_StrnCopy( const char *pSrc, char *pDst, uint32_t maxlen );

uint8_t		cli_ToLower( uint8_t ch );
uint8_t		cli_ToUpper( uint8_t ch );
bool		cli_IsDecimal( const char ch );
bool		cli_IsPrintable( const char ch );
void  		cli_PutHexDigit( uint8_t Nibble );
void  		cli_PutHexByte( const uint8_t Byte );

uint32_t	cli_CmdVersion( uint32_t argc, char **argv );
uint32_t	cli_CmdHelp( uint32_t argc, char **argv );
uint32_t	cli_CmdFoo( uint32_t argc, char **argv );
uint32_t	cli_CmdBar( uint32_t argc, char **argv );
uint32_t	cli_CmdBang( uint32_t argc, char **argv );
uint32_t	cli_CmdHist( uint32_t argc, char **argv );
uint32_t	cli_CmdEcho( uint32_t argc, char **argv );

// These lines may possibly omitted from this include file because they are strictly internal
uint32_t	cli_CmdBufGetInput( void );
void		cli_CmdBufReset( void );
void		cli_CmdBufDelChar( void );
void		cli_CmdBufInsChar( const uint8_t ch );
bool		cli_CmdBufParse( void );
uint32_t	cli_CmdBufHandleCmd( void );

void		cli_HistInit( void );
void		cli_HistAddItem( void );
void		cli_HistStoreCurLine( void );
void		cli_HistGetOlderItem( void );
void		cli_HistGetNewerItem( void );
void		cli_HistCopyToCmdBuf( void );
void		cli_HistCopyFromCmdBuf( void );
void		cli_TabHint( void );
// End of internal use functions

#endif	// _CLI_H_
