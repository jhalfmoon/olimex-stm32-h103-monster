/*
*   version.h
*
*   BUILD_VER_MAJOR is meant to be updated manually
*   BUILD_VER_MINOR is meant to be updated manually
*   BUILD_VER_DEBUG is meant to be updated by a utility, bumpversion.exe,
*   which is executed whenever a "make" or "rebuild" is done.
*/
#ifndef  _VERSION_H_
#define  _VERSION_H_

#define  BUILD_VER_MAJOR  2
#define  BUILD_VER_MINOR  2
#define  BUILD_VER_DEBUG  108

#endif  // _VERSION_H_
