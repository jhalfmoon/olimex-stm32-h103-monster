
#ifndef  _GPIO_H_
#define  _GPIO_H_

#define but_port		GPIOA
#define but_port_rcc	RCC_APB2Periph_GPIOA
#define but_pin			GPIO_Pin_0

#define led_port		GPIOC
#define led_port_rcc	RCC_APB2Periph_GPIOC
#define led_pin			GPIO_Pin_12

void	but_Init( void );
int		but_Read( void );

void	led_Init( void );
void	led_Off( void );
void	led_On( void );
void	led_Toggle( void );

#endif	/* _GPIO_H_ */
