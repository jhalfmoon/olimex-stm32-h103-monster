
#=== ARM Makefile ==============================================

PROJECTNAME = monster
TARGET = STM32
CORE = cortex-m3
FLASH = 128K
SRAM = 20K

VERBOSE = 0
DEBUG = 0
DONGLE = openocd

#=== Defines ====================================================

#used by stm32f10x.h and system_stm32f10x.c to determine which specific drivers to use
DEFINES = STM32F10X_MD
# used by stm32f10x.h to determine if the stdperiph drivers will be used or direct access
DEFINES += USE_STDPERIPH_DRIVER
# The following is a the explicit, but unused counterpart of VECT_TAB_RAM.
# If the latter is defined, then platform_config.h will place the vector table in RAM.
DEFINES += VECT_TAB_FLASH
# used by platform_config.h to determine the pin configuration of the target
DEFINES += USE_OLIMEX_H103
DEFINES += DEBUG=0

#=== Source file and directory definitions ======================

LIB_DIR = lib/
SCRIPT_DIR = ../../script/

EXTERNAL_SOURCES = /1/opslag/projects/ee/arm/external_projects/
STDPERIPH_PATH = $(EXTERNAL_SOURCES)STM32F10x_StdPeriph_Lib_V3.5.0/Libraries/

# CMSIS core code
SRC_FILES += core_cm3
VPATH += $(STDPERIPH_PATH)CMSIS/CM3/CoreSupport

SRC_FILES += system_stm32f10x
VPATH += $(STDPERIPH_PATH)CMSIS/CM3/DeviceSupport/ST/STM32F10x

# CMSIS startup code 
SRC_FILES += startup_stm32f10x_md
VPATH += $(STDPERIPH_PATH)CMSIS/CM3/DeviceSupport/ST/STM32F10x/startup/gcc_ride7

# CMSIS StdPeriph_Driver
SRC_FILES += misc stm32f10x_gpio stm32f10x_rcc stm32f10x_usart
VPATH += $(STDPERIPH_PATH)STM32F10x_StdPeriph_Driver/src/
VPATH += $(STDPERIPH_PATH)STM32F10x_StdPeriph_Driver/inc/

# USB-FS-Device_Driver
# NOTE: We use a patched version of the STM32 USB drivers, because the vanilla version contains some bugs we don't want
SRC_FILES += usb_core usb_init usb_int usb_regs usb_mem
VPATH += src/STM32_USB-FS-Device_Driver-3.3.0/
#VPATH += $(EXTERNAL_SOURCES)STM32_USB-FS-Device_Lib_V3.3.0/Libraries/STM32_USB-FS-Device_Driver/

# User app 
SRC_FILES += main gpio cdc cli stm32f10x_it hw_config usb_desc usb_endp usb_istr usb_prop usb_pwr
VPATH += src/

#=== more definitions ===========================================

include $(SCRIPT_DIR)Makefile-cm3.mk

#=== EOF ========================================================                                                     
